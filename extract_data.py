import pandas as pd
import os
import glob

from readFiles import read_files

def get_new_final_file_name(filename):
    #print(filename)
    new_file_name = filename.split('/')
    new_file_name[1] = 'finalDataset'
    renewed_file_name = '/'.join(new_file_name)
    print(renewed_file_name)
    return renewed_file_name


def extract_relevant_information():
    files_list = read_files()

    for file_name in files_list:
        print('Processing file ', file_name)
        old_dataframe = pd.read_csv(file_name)
        #print(old_dataframe.items)
        new_dataframe = old_dataframe[['CPU rate', 'runtime', 'canonical memory usage',
                                                'total page cache', 'maximum memory usage']]
        #print(new_dataframe)
        filename = get_new_final_file_name(file_name)
        print('Removing File', file_name)
        os.remove(file_name)
        print('Writing output to file', filename)
        new_dataframe.to_csv(filename)

def convert_to_no_index():
    files_list = read_files()
    for file_name in files_list:
        print('Processing file ', file_name)
        old_dataframe = pd.read_csv(file_name)
        filename = get_new_final_file_name(file_name)
        print('Removing File', file_name)
        os.remove(file_name)
        print('Writing output to file', filename)
        old_dataframe.to_csv(filename, index=False)

if __name__ == '__main__':
    #extract_relevant_information()
    convert_to_no_index()