import pandas as pd
import os
from readFiles import read_files

def get_all_dfs():

    files_list = read_files()
    dfs_list = []
    for iteration, file_name in enumerate(files_list):
        curr_dataframe = pd.read_csv(file_name)
        print(' File number: ', iteration,
              ' Current Value: ', len(curr_dataframe))
        del curr_dataframe['Unnamed: 0']
        dfs_list.append(curr_dataframe)

    all_dataframes = pd.concat(dfs_list)
    print(len(all_dataframes))
    return all_dataframes

def count_number_of_entries():
    files_list = read_files()
    dfs_count = 0
    for iteration, file_name in enumerate(files_list):
        curr_dataframe = pd.read_csv(file_name)
        dfs_count = dfs_count + len(curr_dataframe)
        print('File name is: ', file_name)
        print('Number of values: ', dfs_count, ' File number: ', iteration,
              ' Current Value: ', len(curr_dataframe))
    return 0

def get_single_file_data()-> pd.DataFrame:

    filename = './finalDataset/task_usage1/part-00154-of-00500.csv'
    data = pd.read_csv(filename)
    return data

if __name__ == '__main__':
    #get_all_dfs()
    count_number_of_entries()