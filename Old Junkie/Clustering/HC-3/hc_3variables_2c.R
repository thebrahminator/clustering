# Hierarchical Clustering

# Importing the dataset
setwd("~/Shruthi/projects&papers/Research/Files&Code/Cloud/Clustering")
dataset <- read.csv('duration.csv')
X <-dataset[1:10000,2:4]

# Splitting the dataset into the Training set and Test set
# install.packages('caTools')
# library(caTools)
# set.seed(123)
# split = sample.split(dataset$DependentVariable, SplitRatio = 0.8)
# training_set = subset(dataset, split == TRUE)
# test_set = subset(dataset, split == FALSE)

# Feature Scaling
# training_set = scale(training_set)
# test_set = scale(test_set)

# Using the dendrogram to find the optimal number of clusters
dendrogram = hclust(d = dist(X, method = 'euclidean'), method = 'ward.D')
plot(dendrogram,
     main = paste('Dendrogram'),
     xlab = 'Customers',
     ylab = 'Euclidean distances')

# Fitting Hierarchical Clustering to the dataset
hc = hclust(d = dist(X, method = 'euclidean'), method = 'ward.D')
y_hc = cutree(hc, k=2)

# Visualising the clusters
library(cluster)
clusplot(X,
         y_hc,
         lines = 0,
         shade = TRUE,
         color = TRUE,
         labels= 2,
         plotchar = FALSE,
         span = TRUE,
         main = paste('Clusters of tasks_usage'))

library("plot3D")

scatter3D(X$duration, X$memory_use, X$cpu_use, colvar = y_hc, labels = c("Duration","Menory_Use", "CPU_Use"),col = NULL, add = FALSE)

Duration=dataset$duration[1:10000]
Memory_use =dataset$memory_use[1:10000]
Cpu_use =dataset$cpu_use[1:10000]

content = data.frame(Duration,Memory_use,Cpu_use,y_hc)

content2 = content[order(content$y_hc),]

setwd("~/Shruthi/projects&papers/Research/Files&Code/Cloud/Clustering/HC-3")

write.csv(content2, file='sorted_hc_3var_2clusters.csv')

cl_dataset = split(content2, content2$y_hc)

write.csv(cl_dataset[1], file='sorted_hc_3var_2cluster_cluster1.csv')
write.csv(cl_dataset[2], file='sorted_hc_3var_2cluster_cluster2.csv')