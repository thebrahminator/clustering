# Hierarchical Clustering

# Importing the dataset
setwd("~/Shruthi/projects&papers/Research/Files&Code/Cloud/Clustering")
dataset <- read.csv('duration.csv')
X <-dataset[:,2:4]

# Splitting the dataset into the Training set and Test set
# install.packages('caTools')
# library(caTools)
# set.seed(123)
# split = sample.split(dataset$DependentVariable, SplitRatio = 0.8)
# training_set = subset(dataset, split == TRUE)
# test_set = subset(dataset, split == FALSE)

# Feature Scaling
# training_set = scale(training_set)
# test_set = scale(test_set)

# Using the dendrogram to find the optimal number of clusters
dendrogram = hclust(d = dist(X, method = 'euclidean'), method = 'ward.D')
plot(dendrogram,
     main = paste('Dendrogram'),
     xlab = 'Customers',
     ylab = 'Euclidean distances')

# Fitting Hierarchical Clustering to the dataset
hc = hclust(d = dist(X, method = 'euclidean'), method = 'ward.D')
y_hc = cutree(hc, k=4)

# Visualising the clusters
library(cluster)
clusplot(X,
         y_hc,
         lines = 0,
         shade = TRUE,
         color = TRUE,
         labels= 2,
         plotchar = FALSE,
         span = TRUE,
         main = paste('Clusters of tasks_usage'))

c2=dataset$duration[1:1000]
c3=dataset$memory_use[1:1000]
c4=dataset$cpu_use[1:1000]

content = data.frame(c2,c3,c4,y_hc)

content2 = content[order(content$y_hc),]

write.csv(content2, file='sorted_hc.csv')
c1 = content2[1:143,]
c2 = content2[143:496,]
c3 = content2[496:795,]
c4 = content2[795:1002,]
write.csv(c1, file='hc_c1.csv')
write.csv(c2, file='hc_c2.csv')
write.csv(c3, file='hc_c3.csv')
write.csv(c4, file='hc_c4.csv')