#Importing the dataset
setwd("~/Shruthi/projects&papers/Research/Files&Code/Cloud/Clustering")
dataset <- read.csv('duration.csv')
X <-dataset[,3:4]

#Using the elbow method to find the optimal number of clusters
set.seed(8)
wcss = vector()
#for (i in 1:10) wcss[i] = sum(kmeans(dataset, i)$withinss)
for (i in 1:10) wcss[i] = sum(kmeans(X, i)$withinss)
plot(1:10,
     wcss,
     type = 'b',
     main = paste('The Elbow Method'),
     xlab = 'Number of clusters',
     ylab = 'WCSS')

# Fitting K-Means to the dataset
set.seed(29)
#kmeans = kmeans(x = dataset, centers = 4)
kmeans = kmeans(x = X, centers = 4)
print(kmeans)
y_kmeans = kmeans$cluster
print(y_kmeans)
# Visualising the clusters
library(cluster)
#clusplot(dataset,
#         y_kmeans,
#         lines = 0,
#         shade = TRUE,
#         color = TRUE,
#         labels = 2,
#         plotchar = FALSE,
#         span = TRUE,
#         main = paste('Clusters of tasks usage'))

clusplot(X,
         y_kmeans,
         lines = 0,
         shade = TRUE,
         color = TRUE,
         labels = 2,
         plotchar = FALSE,
         span = TRUE,
         main = paste('Clusters of tasks usage'))


library("plot3D")

scatter2D(X$memory_use, X$cpu_use, colvar = y_kmeans, labels = c("Menory_Use", "CPU_Use"),col = NULL, add = FALSE)

Duration=dataset$duration
Memory_use =dataset$memory_use
Cpu_use =dataset$cpu_use

content = data.frame(Duration,Memory_use,Cpu_use,y_kmeans)

content2 = content[order(content$y_kmeans),]

cl_dataset = split(content2, content2$y_kmeans)

setwd("~/Shruthi/projects&papers/Research/Files&Code/Cloud/Clustering/Kmeans-2")

write.csv(content2, file='sorted_kmeans_2var_4cluster.csv')

write.csv(cl_dataset[1], file='sorted_kmeans_2var_4cluster_cluster1.csv')
write.csv(cl_dataset[2], file='sorted_kmeans_2var_4cluster_cluster2.csv')
write.csv(cl_dataset[3], file='sorted_kmeans_2var_4cluster_cluster3.csv')
write.csv(cl_dataset[4], file='sorted_kmeans_2var_4cluster_cluster4.csv')