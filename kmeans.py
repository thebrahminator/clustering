# K-Means Clustering

# Importing the libraries
import numpy as np
from typing import List
import matplotlib.pyplot as plt
import pandas as pd
from merge_files import get_single_file_data
from sklearn.cluster import KMeans

# Importing the dataset
def import_dataset():
    dataset = get_single_file_data()
    del dataset['Unnamed: 0']
    print(dataset)
    return dataset

def split_x(dataset: pd.DataFrame):
    X = dataset.iloc[:10000, [0,1,2,3,4]].values
    print(len(X))
    return X

def kmeans_finding_elbow(dataset: np.ndarray,
           elbow_value_start: int,
           elbow_value_end: int):

    wcss = []
    for i in range(elbow_value_start, elbow_value_end+1):
        print(f'Starting value: {i}')
        kmn = KMeans(n_clusters = i, init= 'k-means++', max_iter= 300, n_init= 10, random_state= 0)
        kmn.fit(X=dataset)
        print(kmn.inertia_)
        wcss.append(kmn.inertia_)

    return wcss

def kmeans_after_elbow(dataset: np.ndarray,
                       clusters: int):
    kmn = KMeans(n_clusters=clusters, init='k-means++', max_iter=300, n_init=10, random_state= 0)
    y = kmn.fit_predict(X=dataset)
    return y

def plot_graph(wcss: List,
               elbow_value_start: int,
               elbow_value_end: int):

    plt.plot(range(elbow_value_start, elbow_value_end+1), wcss)
    plt.title('Elbow Method')
    plt.xlabel('Number of Clusters')
    plt.ylabel('WCSS')
    plt.show()

def plot_scatter_graph():
    pass
if __name__ == '__main__':
    data = import_dataset()
    input_data = split_x(dataset=data)
    wcss_list = kmeans_finding_elbow(dataset=input_data,
                        elbow_value_start=500,
                        elbow_value_end=600)
    plot_graph(wcss_list, elbow_value_start=500, elbow_value_end=600)

    kmeans_val = kmeans_after_elbow(dataset=input_data, clusters=585)