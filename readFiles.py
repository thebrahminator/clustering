import pandas as pd
import glob
import csv
import os

def read_files():
    file_list = glob.glob('./finalDataset/task_usage/*.csv')
    print(len(file_list))
    print(file_list)
    return file_list

def reading_csv(file_list):
    flag = 1
    for file_name in file_list:
        file_content = pd.read_csv(file_name)
        print(file_content.items)
        flag = flag + 1
        if flag == 3:
            break

def convert_files_to_header():
    file_headings = pd.read_csv('schema.csv')
    headings = file_headings['content'].tolist()
    print(headings)
    file_list = read_files()
    for file_name in file_list:
        with open(file_name, newline='') as f:
            print('opening file ', file_name)
            r = csv.reader(f)
            data = [line for line in r]
        os.remove(file_name)
        print('Removed file', file_name)
        new_filename = get_new_file_name(file_name)
        with open(new_filename, 'w', newline='') as k:
            print('writing to file ', new_filename)
            w = csv.writer(k)
            w.writerow(headings)
            w.writerows(data)

    return 1


def get_new_file_name(filename):
    #print(filename)
    new_file_name = filename.split('/')
    new_file_name[1] = 'properDataset'
    renewed_file_name = '/'.join(new_file_name)
    print(renewed_file_name)
    return renewed_file_name

def get_new_dropped_file_name(filename):
    #print(filename)
    new_file_name = filename.split('/')
    new_file_name[1] = 'droppedDataset'
    renewed_file_name = '/'.join(new_file_name)
    print(renewed_file_name)
    return renewed_file_name

def convert_to_runtime():
    list_of_files = read_files()
    for file_name in list_of_files:
        print('Opening File ', file_name)
        cluster_data = pd.read_csv(file_name)
        cluster_data['runtime'] = cluster_data['end time'] - cluster_data['start time']
        cluster_data = cluster_data.drop(['job ID', 'task index',
                           'machine ID', 'aggregation type', 'end time', 'start time'], axis=1)
        new_file_name = get_new_dropped_file_name(file_name)
        print('Printing to filename ', new_file_name)
        os.remove(file_name)
        cluster_data.to_csv(new_file_name, index=False)

if __name__ == '__main__':
    #read_files()
    #convert_files_to_header()
    convert_to_runtime()
    #get_new_file_name('./dataset2/task_usage/part-00167-of-00500.csv')
    #files_list = read_files()
    #reading_csv(file_list=files_list)
